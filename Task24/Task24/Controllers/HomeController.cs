﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            Supervisor supervisor1 = new Supervisor();
            supervisor1.Name = "Tom Max";
            supervisor1.Id = 1;
            supervisor1.Level = "Pro";
            supervisor1.IsAvailable = true;
            SupervisorGroup.AddSupervisor(supervisor1);

            Supervisor supervisor2 = new Supervisor();
            supervisor2.Name = "Sam Jack";
            supervisor2.Id = 2;
            supervisor2.Level = "Pro";
            supervisor2.IsAvailable = true;
            SupervisorGroup.AddSupervisor(supervisor2);
            
            Supervisor supervisor3 = new Supervisor();
            supervisor3.Name = "Jim Hans";
            supervisor3.Id = 3;
            supervisor3.Level = "Pro";
            supervisor3.IsAvailable = true;
            SupervisorGroup.AddSupervisor(supervisor3);

            Supervisor supervisor4 = new Supervisor();
            supervisor4.Name = "Joe Obama";
            supervisor4.Id = 4;
            supervisor4.Level = "Pro";
            supervisor4.IsAvailable = false;
            SupervisorGroup.AddSupervisor(supervisor4);

            ViewBag.Message =  DateTime.Now.ToString(); 
            return View("MyFirstView");
        }


        [HttpGet]
        public IActionResult SupervisorInfo()
        {

            return View();
        }
        [HttpPost]
        public IActionResult SupervisorInfo(Supervisor supervisor)

        {
            if (ModelState.IsValid)
            {
                SupervisorGroup.Supervisors.Add(supervisor);
                return View("AddSupervisorConfirmation", supervisor);
            }
            else
            {
                return View();
            } 
        }
        [HttpGet]
        public IActionResult AllSupervisors()

        {
            return View(SupervisorGroup.Supervisors);
        }

    }
}
